#!/usr/bin/python2.7
'''
Small utility to revert an ESX VM to a particular snapshot via SSH rather than
using the vSphere SDK which has license constraints. 

The SSH password can be obtained from an environment variable to avoid being stored 
in scripts. The name of the environment variable should be in the format 
"PASSWD_HOSTNAME". Where HOSTNAME is the upper case hostname of the ESX host. For 
FQDN's and IP addresses the periods should be replaced with underscores.

    
Dependencies
------------
The following Python modules are required and must be installed before using VMRevert:

pycrypto - http://www.voidspace.org.uk/downloads/pycrypto26/pycrypto-2.6.win32-py2.7.exe
paramiko - https://pypi.python.org/packages/source/p/paramiko/paramiko-1.10.1.tar.gz


Example Usage
-------------

Command line:
vmRevert.py esxserver.local root "My VM" "clean install snapshot"

Python Module:
from vmRevert import VMRevert
vm = VMRevert('esxserver.local', 'root', 'My VM')
vm.revert('clean install snapshot')


@author: icamp
'''
import argparse
import os
import sys
import paramiko
import getpass
import textwrap
import re



class VMRevertError(Exception):
    pass


class VMRevert(object):
    '''
    Small utility to revert an ESX VM to a particular snapshot. Raises a 
    VMRevertError exception if unsuccessful.
    '''
    
    def __init__(self, esxHost, username, password, vm, quiet=True):
        self.client = paramiko.SSHClient()
        
        self.esxHost = esxHost
        self.username = 'root'               
        self.vm = vm
        self.quiet = quiet
        self.suppliedPassword = password

    
    def revert(self, snapshot):
        if (self.quiet == False):
            doRevert = raw_input('Revert VM "%s" to snapshot "%s"? (y/n)' % (self.vm, snapshot))
            if ('n' in doRevert.lower()):
                raise VMRevertError('Revert aborted')        
        
        print('Reverting VM "%s" to snapshot "%s" on host %s...' % (self.vm, snapshot, self.esxHost))     
        try:
            self.__connect()
            vmId = self.__getVMId()
            snapshotId = self.__getSnapshotId(vmId, snapshot)
            self.__revertToSnapshot(vmId, snapshotId)
        
        finally:
            self.__disconnect()
            
            
    def __getPassword(self):
        if (self.suppliedPassword is not None):
            return self.suppliedPassword
        
        elif (self.quiet):
            return self.__getPasswordFromEnv()
        
        else:
            return self.__getPasswordFromConsole()           
        
            
    def __getPasswordFromConsole(self):
        try:
            return getpass.getpass()
        
        except:
            raise VMRevertError('Error: Unable to obtain password from console')        
            
    
    def __getPasswordFromEnv(self):
        passwordVarName = 'PASSWD_' + self.server.split('.')[0].upper();
        if not (os.environ.has_key(passwordVarName)):
            raise VMRevertError('Error: Unable to find password in an environment variable called "' + passwordVarName + '"')            
        
        return os.environ[passwordVarName]            
        
                
    def __connect(self):
        '''
        Connect to the ESX host using SSH. The hosts host key is not validated.
        '''
        self.client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        try:        
            self.client.connect(self.esxHost, username=self.username, password=self.__getPassword())
        except Exception, err:
            raise VMRevertError('Failed to connect to ESX host: ' + str(err))
        
        
    def __executeCmd(self, cmd):
        '''
        Execute the supplied command and return its stdout as a list. Raises an 
        exception if the command prints to stderr.
        '''        
        stdin, stdout, stderr = self.client.exec_command(cmd)
        
        errLines = stderr.readlines()
        if (len(errLines) > 0):
            raise VMRevertError(''.join(errLines))
        
        else:
            return stdout.readlines()         
        
        
    def __disconnect(self):
        self.client.close()
        
        
    def __getVMId(self):
        '''
        Example Command:
        vim-cmd vmsvc/getallvms
        
        Example Output:
        17   linux-vm      [datastore1] linux-vm/vm.vmx      linuxGuest   vmx-08    some comment
        '''
        lines = self.__executeCmd('vim-cmd vmsvc/getallvms')
        
        listRegEx = re.compile('^(\d+)\s+(.+?)\s+(\[(\S+)\]\s(.+)/.+\.vmx)\s')
        for line in lines:
            match = listRegEx.match(line)
            if match:
                if match.group(2) == self.vm:
                    return match.group(1)
                
        raise VMRevertError('Error: Unable to find VM Id for VM: ' + self.vm)
            
            
    def __getSnapshotId(self, vmId, snapshot):
        '''
        Example Command:
        vim-cmd vmsvc/snapshot.get "17" | grep -A1 ": Clean VM$"
        
        Example Output:
        --Snapshot Name        : Clean VM
        --Snapshot Id        : 38
        '''
        lines = self.__executeCmd('vim-cmd vmsvc/snapshot.get "%s" | egrep -A1 ": %s$"' % (vmId, snapshot))
        
        if (len(lines) == 2):            
            snapshotName = lines[0].split(":", 1)
            
            if (len(snapshotName) == 2 and 'Snapshot Name' in snapshotName[0] and snapshotName[1].strip() == snapshot):                
                snapshotId = lines[1].split(":", 1)
                
                if (len(snapshotId) == 2 and 'Snapshot Id' in snapshotId[0]):                    
                    return snapshotId[1].strip()
        
        raise VMRevertError('Error: Unable to find Snapshot Id for snapshot: ' + snapshot)
        
    
    def __revertToSnapshot(self, vmId, snapshotId):
        '''
        Example Command:
        vim-cmd vmsvc/snapshot.revert 17 38 yes
        
        If successful a list of all snapshots is returned. A success message is
        not returned.
        '''
        self.__executeCmd('vim-cmd vmsvc/snapshot.revert "%s" "%s" yes' % (vmId, snapshotId))            

                
        
def main():

    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
                                     description='Small utility to revert an ESX VM to a particular snapshot.', 
                                     epilog=textwrap.dedent('''\
    Password options:
    
    1) Interactively (default)
    By default the user will be prompted to enter the password after executing the command.
    
    2) Environment variable
    If you need to automate reverting a VM from within a script the password required for the 
    ESX server can be placed within an environment variable to avoid storing the password in 
    a script. The name of the environment variable should be in the format "PASSWD_HOSTNAME". 
    Where HOSTNAME is the upper case hostname of the ESX host. For FQDN's and IP addresses 
    the periods should be replaced with underscores.
    
    3) Command line (not recommended)
    The password can be specified on the command line using the optional -p option.'''))
                                              
    parser.add_argument('-q', '--quiet', action='store_true')                            
    parser.add_argument('server', help="Hostname of the ESX server where the VM resides.")
    parser.add_argument('username', help="ESX username to connect with.")
    parser.add_argument('vm', help='Name of VM to revert.')
    parser.add_argument('snapshot', help='Name of snapshot to revert to.')
    parser.add_argument('-p', '--password')

    args = parser.parse_args()
    
    try:
        vm = VMRevert(args.server, args.username, args.password, args.vm, args.quiet)
        vm.revert(args.snapshot)
    
    except VMRevertError, e:
        sys.stderr.write(str(e) + '\n')
        sys.exit(1);

                      
if  __name__ =='__main__':
    main()
