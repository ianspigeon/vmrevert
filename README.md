## VMRevert
Small utility to revert an ESX VM to a particular snapshot via SSH rather than
using the vSphere SDK which has license constraints. 

The SSH password can be obtained from an environment variable to avoid being stored 
in the scripts. The name of the environment variable should be in the format 
"PASSWD_HOSTNAME". Where HOSTNAME is the upper case hostname of the ESX host. For 
FQDN's and IP addresses the periods should be replaced with underscores.


#### Example Usage
Command line:<br>
vmRevert.py esxserver.local root "My VM" "clean install snapshot"

Python Module:<br>
from vmRevert import VMRevert<br>
vm = VMRevert('esxserver.local', 'root', 'My VM')<br>
vm.revert('clean install snapshot')


#### Password options
1. Interactively (default)<br />
By default the user will be prompted to enter the password after executing the command.

2. Environment variable<br />
If you need to automate reverting a VM from within a script the password required for the 
ESX server can be placed within an environment variable to avoid storing the password in 
a script. The name of the environment variable should be in the format "PASSWD_HOSTNAME". 
Where HOSTNAME is the upper case hostname of the ESX host. For FQDN's and IP addresses 
the periods should be replaced with underscores.

3. Command line (not recommended)<br />
The password can be specified on the command line using the optional -p option.


#### Dependencies
The following Python modules are required and must be installed before using RevertVM:

* pycrypto - http://www.voidspace.org.uk/downloads/pycrypto26/pycrypto-2.6.win32-py2.7.exe 
* paramiko - https://pypi.python.org/packages/source/p/paramiko/paramiko-1.10.1.tar.gz
